import matplotlib.pyplot as plt
import numpy as np

def plot_threshold_finetuner(df_main, feature1='ht_res_freqsplit', feature2='TC_RT_UDF_SET_Trim_UDF_QA_Fine', nrows=3, ncols=3, th_start=0.2, th_stop=2.5):
    fig, ax = plt.subplots(nrows=nrows, ncols=ncols, squeeze=False) # two axes on figure
    fig.set_figheight(ncols*5)
    fig.set_figwidth(ncols*5)

    index = 0

    THRESHOLDs = np.linspace(start=th_start, stop=th_stop, num=nrows*ncols)
    for threshold in THRESHOLDs:
        inliers, outliers = [], []

        for idx in range(len(observations)):
            if err_list[idx] < threshold:
                inliers.append(idx)
            else:
                outliers.append(idx)

        print(f'for THRESHOLD value: {threshold} ->  nr. of INLIERS: {len(inliers)}, nr. of OUTLIERS: {len(outliers)}')

        ax[index//ncols][index%nrows].plot(df_main[feature1][inliers], df_main[feature2][inliers], '.g')
        ax[index//ncols][index%nrows].plot(df_main[feature1][outliers], df_main[feature2][outliers], '.r')
        len_out = len(outliers)
        len_in = len(inliers) # LOL
        ratio = np.round(len_in / (len_out+len_in),3)
        ax[index//ncols][index%nrows].title.set_text(f'TH:{np.round(threshold,3)}, ALL/IN ratio:{ratio}')
        index+=1   