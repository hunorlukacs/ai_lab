from BMA_FUZZY.Input import Input
from BMA_FUZZY.helpers.helper_functions.get_boundaries import get_boundaries


def input_set_fitData(inp:Input, observations, desired_outputs) -> Input:
  '''
    Creates an Input object from a Config object.

    Parameter:
    -------
    conf: Config object

    Returns:
    ------
    inp: Input object

    Notes:
    -------
    All the attributes from Config object will be set or added to the Input object.

  '''
  inp_new = Input()
  inp_new = inp

  inp_new.observations = observations
  inp_new.desired_outputs = desired_outputs
  inp_new.observation_dim = observations.shape[1]
  inp_new.boundaries = get_boundaries(obs=observations, desired_output=desired_outputs)
  
  return inp_new