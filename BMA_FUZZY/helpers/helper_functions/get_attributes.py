from operator import attrgetter
from BMA_FUZZY.Input import Input

def get_attributes(attributes, inp:Input):
  stringified = [f'{attr}' for attr in attributes]
  return attrgetter(stringified)(inp)