import numpy as np

def antes(rule: np.ndarray) -> np.ndarray:
  """ Returns the antecedents of the rule system """
  return rule[:-1]