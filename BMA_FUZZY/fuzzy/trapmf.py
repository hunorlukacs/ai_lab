import numpy as np

def trapmf(x:np.array, abcd:list) -> np.array:
  """
  Trapezoidal membership function

  Parameters
  ----------
  x: 1d array
    Independent var
  abcd: 1d array, length 4
    [a, b, c, d] Breakpoints. Ensure that a <= b <= c <= d.
  
  Returns
  -------
  y: 1d array
    Output values for the x values, according to the membership function  
  
  Example:
  >>> trapmf(np.array([1.5]), [1,2,3,5])

  """

  a, b, c, d = np.r_[abcd]
  assert a <= b <= c <= d, '[-] Ensure that a <= b <= c <= d! The values are: a={}, b={}, c={}, d={}'.format(a,b,c,d)

  y = np.zeros(len(x)) # 0 if x < a or d < x

  idx = np.nonzero(np.logical_and(a < x, x < b))[0] 
  y[idx] = (x[idx] - a) / float(b - a)

  idx = np.nonzero(np.logical_and(b <= x, x <=c))[0]
  y[idx] = 1

  idx = np.nonzero(np.logical_and(c < x, x < d))[0]
  y[idx] = (d - x[idx]) / float(d - c)

  return y

