import numpy as np

def error_L2(Ys, Ds):
  '''
    Calculates the L2 norm.

    Parameters:
    ----------
      Ys: (N, )
          Predictions
      Ds: (N, )
          Desired outputs
    
    Returns:
    --------
      e: scalar
          L2 norm
  '''
  e = np.linalg.norm(Ys - Ds)
  return e

def error_L2_piecewise(Ys, Ds):
  return Ys-Ds
