import numpy as np
from BMA_FUZZY.fuzzy.error_helpers.error_L2 import error_L2, error_L2_piecewise
from BMA_FUZZY.fuzzy.error_helpers.error_canberra import error_canberra, error_canberra_piecewise


def error(Ys, Ds, metric=None):
  ''' Decides which metric to use ''' 

  if metric == None:
    current_metric = 'L2'
  else:
    current_metric = metric

  if current_metric == 'Canberra':
    return error_canberra(Ys, Ds)
  elif current_metric == 'L2':
    return error_L2(Ys, Ds)
  else:
    raise ValueError(f'[-] There is no such metric!(Available metrics: Canberra, L2)')



def error_piecewise(Ys, Ds, metric=None):
  if metric == None:
    current_metric = 'L2'
  else:
    current_metric = metric

  if current_metric == 'Canberra':
    return error_canberra_piecewise(Ys, Ds)
  elif current_metric == 'L2':
    return error_L2_piecewise(Ys, Ds)
  else:
    raise ValueError(f'[-] There is no such metric!(Available metrics: Canberra, L2)')
