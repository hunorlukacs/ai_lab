import numpy as np

def error_canberra(Ys, Ds):
  e = abs(Ys - Ds) / ( abs(Ys) + abs(Ds) )
  return np.sum(e)

def error_canberra_piecewise(Ys, Ds):
  return abs(Ys - Ds) / ( abs(Ys) + abs(Ds) )
