import numpy as np
from fuzzy.w_min import *

# MamdaniInference
def mamdaniInference(frbs:np.ndarray, observations:np.ndarray) -> np.ndarray:
  """
  Predict output values for the observations, using Mamdani Inference and Center of Gravity method.
  We're using the explicit formula.
  The length of the Outputs will be the same as length of the Observations.
  
  Parameters:
  ----------
  frbs: Fuzzy rule-based system
  observastions: Observations. Dimensionality of the observations must match with the number of Antecedents in a Rule.

  Result:
  -----
  Ys: Output values for the observations accoring to the frbs  

  Example:
  >>> frbs = np.array([[[1,2,2,3],[1,3,3,5],[3,4,4,5]], [[2,4,4,6],[1,2,2,3],[4,5.5,5.5,7]]])
  >>> obs = np.array([[2.6,2.4]])
  >>> mamdaniInference(frbs=frbs, observations=obs)
  """


  _observation_len = observations.shape[0]

  Ys = np.zeros(len(observations))

  w_mins = np.empty((0, _observation_len), float)

  w_mins = np.array([np.vstack([w_mins, w_min(rule=rule, observations=observations)]) for rule in frbs])
  w_mins = np.reshape(w_mins, (frbs.shape[0], _observation_len))

  consequents_mtx = frbs[:,-1]

  A = np.array([consequents_mtx[:,0]]).T
  B = np.array([consequents_mtx[:,1]]).T
  C = np.array([consequents_mtx[:,2]]).T
  D = np.array([consequents_mtx[:,3]]).T

  _numerator_mtx = 3*w_mins * (D**2 - A**2) * (1-w_mins) + 3 * w_mins**2 * (C*D - A*B) + w_mins**3 * (C-D+A-B)*(C-D-A+B)
  _denominator_mtx = 2*w_mins * (D-A) + w_mins**2 * (C + A - D - B)

  _numerator = np.sum(_numerator_mtx, axis=0)
  _denominator = np.sum(_denominator_mtx, axis=0)
  Ys = np.divide(_numerator, _denominator, out=np.zeros_like(_numerator), where=_denominator!=0)
  Ys *= (1/3)
  return Ys
