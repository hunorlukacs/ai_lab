import numpy as np
from fuzzy.helpers import antes

from fuzzy.trapmf import trapmf


def w_min(rule, observations):
  """
  W_min calculator

  Parameters
  ----------
  observations: nd array, size: (dim of input) x (number of input) 
    Observations

  Returns
  -------
  w: 1d array, size: number of inputs (observations)
    W_min for each input

  Example:
  >>> frbs = np.array([[[1,2,2,3],[1,3,3,5],[3,4,4,5]], [[2,4,4,6],[1,2,2,3],[4,5.5,5.5,7]]])
  >>> obs = np.array([[1.5,2], [4,2]])
  >>> w_min(rule=frbs[0], observations=obs) # [0.5 0.]
  >>> w_min(rule=frbs[1], observations=obs) # [0. 1.]
  """
  obs = observations.T
  tmp = list(map(lambda ante, obs: trapmf(obs, ante), antes(rule), obs))
  return np.array(list(map(lambda out: min(out), np.array(tmp).T)))
