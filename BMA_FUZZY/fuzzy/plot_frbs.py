import copy
from matplotlib import pyplot as plt
import numpy as np
from BMA_FUZZY.fuzzy.trapmf import trapmf
from BMA_FUZZY.fuzzy.w_min import w_min

def plot_frbs(frbs, boundaries, obs=None, PADDING_RATE=0):
  ''' 
  Plotting fuzzy rule-based system.
  
  Parameters:
  ----------
  frbs: Fuzzy rule-based system
  boundaries: Lower and upper boundaries for each membership function in a rule. For all Antecedents and for the Consequent
  obs: Observations. Dimensionality of the observations must match with the number of Antecedents in a Rule.

  Result:
  ---
  Plots the membership function for the FRBS together with the observations.

  Examples
  --------
  This function can be used as follows:
  >>> frbs = np.array([[[1,2,2,3],[1,3,3,5],[3,4,4,5]], [[2,4,4,6],[1,2,2,3],[4,5.5,5.5,7]]])
  >>> boundaries = [[0,7], [0,7], [0,7]]
  >>> obs = np.array([[2.6,2.4]])
  >>> plot_frbs(frbs, boundaries, obs=obs)
  '''

  colors = ['green', 'orange', 'yellow', 'blue']
  colors_obs = ['cyan', 'purple',  'magenta', 'red']

  nr_rules, nr_trapmf, _ = frbs.shape
  fig, ax = plt.subplots(nrows=nr_rules, ncols=nr_trapmf, squeeze=False) # two axes on figure
  fig.set_figheight(3*nr_rules)
  fig.set_figwidth(5*nr_trapmf)

  # set the spacing between subplots
  fig.subplots_adjust(left=0.1,bottom=0.1, right=0.9, top=0.9, wspace=0.4, hspace=0.4)
  
  # bounds = copy.deepcopy(boundaries)
  lower_bounds, upper_bounds = np.min(frbs, axis=(2,0)), np.max(frbs, axis=(2,0))
  bounds = np.array([lower_bounds, upper_bounds]).T

  for idx in range(len(bounds)):
    dist = bounds[idx][1] - bounds[idx][0]
    bounds[idx][0] -= PADDING_RATE * dist
    bounds[idx][1] += PADDING_RATE * dist
  
  X = []
  for b in bounds:
    X.append(np.linspace(b[0],b[1],1000))

  isLegend = True
  for _rule_idx, _rule in enumerate(frbs):
    for _feature_idx, _trapmf in enumerate(_rule):
      y = trapmf(x=X[_feature_idx], abcd=_trapmf)
      ax[_rule_idx][_feature_idx].plot(X[_feature_idx], y, color=colors[_feature_idx%len(colors)])
      if _feature_idx < nr_trapmf-1: 
        ax[_rule_idx][_feature_idx].set_title(f'Rule[{_rule_idx}], Ante[{_feature_idx}]')
        # antecedents
        if obs is not None:
          for _obs_idx, _obs in enumerate(obs):
            mu = trapmf(x=np.array([_obs[_feature_idx]]), abcd=_trapmf)
            ax[_rule_idx][_feature_idx].vlines(x=_obs[_feature_idx], ymin=0, ymax=mu, color=colors_obs[_obs_idx%len(colors_obs)], linestyles='dashed')
            ax[_rule_idx][_feature_idx].hlines(y=mu, xmin=_obs[_feature_idx], xmax=bounds[_feature_idx][1], color=colors_obs[_obs_idx%len(colors_obs)], linestyles='dashed')
      else:
        # consequent
        ax[_rule_idx][-1].set_title(f'Rule[{_rule_idx}], Cons')
        if obs is not None:
          w = w_min(_rule, obs)
          a,b,c,d = _rule[-1][0], _rule[-1][1], _rule[-1][2], _rule[-1][3]
          colors_cons = [colors_obs[i%len(colors_obs)] for i in range(len(w))]
          labels_cons = [f'obs[{i}]' for i in range(len(w))]
          for w_idx in range(len(w)):
            if isLegend:
              ax[_rule_idx][-1].hlines(y=w[w_idx], xmin=(w[w_idx] * (b-a)+a), xmax=-1*(w[w_idx]*(d-c)-d), color=colors_cons[w_idx], linestyles='dashed', label=f'obs[{w_idx}]')
            else:
              ax[_rule_idx][-1].hlines(y=w[w_idx], xmin=(w[w_idx] * (b-a)+a), xmax=-1*(w[w_idx]*(d-c)-d), color=colors_cons[w_idx], linestyles='dashed')

          isLegend = False


  # fig.legend((l1, l2), labels, 'upper left')
  fig.legend()
  fig.show()