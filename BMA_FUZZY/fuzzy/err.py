import math
import numpy as np
from BMA_FUZZY.Input import Input
from BMA_FUZZY.fuzzy.error_helpers.error import error
from BMA_FUZZY.fuzzy.mamdani_inference import mamdaniInference

def err(individum, inp:Input, indeces=None, metric=None) -> float:
  ''' Returns the error (one value) depending on the chosen metric and filtering indeces'''
  
  frbs = individum.model

  final_loss = 0 
  if indeces is None:
    final_loss = error(Ys=mamdaniInference(frbs=frbs, observations=inp.observations), Ds=inp.desired_outputs, metric=metric)
  else:
    Ys = mamdaniInference(frbs=frbs, observations=inp.observations[indeces])
    Ds=inp.desired_outputs[indeces]
    final_loss = error(Ys=Ys, Ds=Ds, metric=metric)

  individum.error = final_loss
  return final_loss