from BMA_FUZZY.Input import Input
from BMA_FUZZY.bma.bacterium_module.Bacterium import Bacterium
# from BMA_FUZZY.helpers.bma_optimizer import bacterialMemeticEvolution
from BMA_FUZZY.bma.bma_optimizer import BMA_optimizer
from BMA_FUZZY.bma.population_module.Population import Population
from BMA_FUZZY.fuzzy.helpers import antes
from BMA_FUZZY.fuzzy.trapmf import trapmf
from BMA_FUZZY.model_save_load import save_model, load_model
from bea.Input import input_set_fitData

class BacterialMemeticAlg4FuzzyRBS():
  def __init__(self, inp=Input) -> None:
    '''
    Linear regression based on Fuzzy rule-based system, optimized with the bacterial memetic algorithm.

    Example:
    -------
    >>> inp = Input()
    >>> bma = BacterialMemeticAlg4FuzzyRBS(conf=conf)
    >>> bma.fit(observations=observations, desired_outputs=desired_outputs)
    >>> sol = bma.get_solution() # Bacterium object. 
    >>> sol.model  # in order to extract the Fuzzy rules
    >>> # sol.plot_fuzzyRBS()  # plotting the Fuzzy rules
    '''
    self.inp = Input() if inp == None else inp 
    self.solution, self.population = None, None
    self._fittingIsDone_orLoaded = False

  def fit(self, observations, desired_outputs):
    ''' Fit linear model. 
      
      Parameters:
      ----------
      observations: (N, O)
      desired_outputs: (N, )
      
       - N: number of samples
       - O: observation dimenstion

    '''
    assert len(observations) == len(desired_outputs), 'The length \
      of the observations and the length of the desired_outputs has to match.\
      Current lengths: observations: {}, desired_outputs: {}'.format(len(observations), len(desired_outputs))
    
    desired_outputs = desired_outputs.reshape((len(desired_outputs), ))
    # Updates the Input object according to the configuration. This will be the input of the evolutionary algorithm. 
    self.inp = input_set_fitData(self.inp, desired_outputs=desired_outputs, observations=observations)

    bma = BMA_optimizer(inp=self.inp, MyBacteriumConcreteClass=Bacterium, MyPopulationConcreteClass=Population)
    bma.fit(observations=self.inp.observations, desired_outputs=self.inp.desired_outputs)

    self.solution, self.population = bma.solution, bma.population

    self._fittingIsDone_orLoaded = True

  def get_solution(self) -> Bacterium:
    if not self._fittingIsDone_orLoaded:
      raise ValueError('[-] There is no solution yet. Please call the fit function first!')
    return self.solution
    
  def predict(self, Xs):
    if not self._fittingIsDone_orLoaded:
      raise ValueError('[-] There is no solution yet. Please call the fit function first!')
    return self.solution.predict(Xs=Xs)

  def explain(self, observations):
    '''
      Parameters:
      -------
      observations: (N, O)
    '''
    obs = observations.T
    for rule in self.solution.model:
      print('rule: ', rule)
      tmp = list(map(lambda ante, obs: trapmf(obs, ante), antes(rule), obs))
      print('tmp: ', tmp)

  def save(self, path=None, filename=None):
    '''
      Saves all the data into a file.

      Keys: 
      -----
      - `population`: List[Bacterium]
          -  The whole population.
      - `solution`: Bacterium
          - Best individual
      - `conf`: Config
          - Configuration file. 

      Example:
      -------
      Save:
      >>> bma.save(path=f'{ROOT_DIR}/data/models/', filename='test')
      >>> # [+] Model successfully saved to: {ROOT_DIR}/data/models//test_20221202-232101.model
      >>> print(bma.get_solution().get_err())   # 0.3291587218408204
      Load:
      >>> b2 = BacterialMemeticAlg4FuzzyRBS(conf=Config())
      >>> b2.load(path='{ROOT_DIR}/data/models//test_20221202-232101.model')  # [+] Model loaded successfully!
      >>> b2.get_solution().get_err() # 0.3291587218408204


    '''
    if path is None:
      path=f'./data/models/'
    if filename is None:
      filename = 'test'

    data = {}
    data['population'] = self.population
    data['solution'] = self.solution
    data['inp'] = self.inp

    save_model(data=data, directory=path, filename=filename, append_time=True)
    return True

  def load(self, path, set_input=False):
    '''
      Loads all the data from a dictionary.

      Keys: 
      -----
      - `population`: List[Bacterium]
          -  The whole population.
      - `solution`: Bacterium
          - Best individual
      - `conf`: Config
          - Configuration file. 

      See also: save()
    '''
    data = load_model(path=path)
    self.population = data['population']
    self.solution = data['solution']
    if set_input:
      self.inp = data['inp']
    self._fittingIsDone_orLoaded = True 
    print('[+] Model loaded successfully!')
    return True