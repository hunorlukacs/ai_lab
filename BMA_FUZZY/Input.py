
from BMA_FUZZY.helpers.helper_functions.get_boundaries import get_boundaries
from bea.Input import InputBEA

class Input(InputBEA):
  ''' Config parameters for Bacterial Memetic Algorthm for Fuzzy Rule-Based System '''
  n_gen = 10
  n_ind = 10
  n_clone = 3
  n_inf = 10

  nr_rules = 1

  THETA = .1
  lm_prob = .2
  lm_iter = 10
  gamma_init = .5
  b_mut=True  # enable bacterial mutation
  b_gt=True   # enable bacterial gene transfer
  b_lm=True   # enable the levenberg marquardt on bacterial evolution

  boundaries = None
  observations = None
  observation_dim = None
  desired_outputs = None

  PADDING_RATE = 0.3
  # METRIC_DISTANCE = 'Canberra'
  METRIC_DISTANCE = 'L2'
  BACT_MUT_DIVIDE_GENELIST_INTO = 3
  SAVE_DIR = 'data/models/'

def input_set_fitData(inp:Input, observations, desired_outputs):
  '''
    Creates an Input object from a Config object.

    Parameter:
    -------
    conf: Config object

    Returns:
    ------
    inp: Input object

    Notes:
    -------
    All the attributes from Config object will be set or added to the Input object.

  '''
  inp_new = Input()
  inp_new = inp

  inp_new.observations = observations
  inp_new.desired_outputs = desired_outputs
  inp_new.observation_dim = observations.shape[1]
  inp_new.boundaries = get_boundaries(obs=observations, desired_output=desired_outputs)
  
  return inp_new