import numpy as np
from BMA_FUZZY.Input import Input
from bea.bacterium_modul.BacteriumAbstract import BacteriumAbstract


class Bacterium(BacteriumAbstract):
  '''
    Representation of a bacterium.
    
    Attributes:
    --------
    model: (R, A+1, 4) ndarray 
      Contains all the data for the FRBS representation
    error: float
      (Init value: nan) The error value associated to the model, specified in the data attribute

    Notations:
    --------
    R - number of rules, A - number of antecedents
  '''

  def __init__(self, inp: Input) -> None:
    # init for BEA
    super().__init__(inp)
    
    self.inp = inp
    self._model = self.create_model()
    self._error = np.nan

  ### Implment abstract methods for BEA ###
  from ._abstract_functions import create_model, get_chromosome_length, gene_mutation, get_err, set_genes, get_genes
  ### Implment abstract methods for BEA ###
  
  ### Additional Imported methods ###
  from ._helper_functions import plot_fuzzyRBS, predict
  from ._levenberg_marquardt import levenberg_marquardt
  from ._exlpicit_formula import explicit_formula_predictions
  from ._jacobian_matrix import jacobian_matrix_analytic_self, jacobian_matrix_numeric
  ### Additional Imported methods  ###

  def mutation(self):
    ''' 
      Performs the Bacterial Mutation operation on the bacterial individual.   
      Returns: Self

      Example:
      --------

      >>> # error before:  3.18702007247512
      >>> bact.mutation()
      >>> # clone errors:  [3.18702007247512, 1.8139090649201346]
      >>> # clone errors:  [1.8139090649201346, 2.9233804170085724]
      >>> # error after:  1.8139090649201346
    '''
    return super().mutation()

  