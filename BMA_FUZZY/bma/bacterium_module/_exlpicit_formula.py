import numpy as np
from BMA_FUZZY.fuzzy.w_min import w_min

def get_numerator_and_denominator_independent(frbs, X):
  ''' 
    According to the explicit formula, it calculates the numerator and the denominator.

    Input:
    -------
      frbs: (R, O+1, 4)
           Fuzzy Rule-Based System
      X: (N, O)
         Observations

    Returns:
    --------
      (Nu, De): tuple
          Nu: scalar (numerator)
          De: scalar (denominator)

    Note:
    -------
      R - nr rules, O - observation dimension, N - number of observations
  '''

  Nu, De = 0,0
  _observation_len = X.shape[0]

  w_mins = np.empty((0, _observation_len), float)
  w_mins = np.array([np.vstack([w_mins, w_min(rule=rule, observations=X)]) for rule in frbs])
  w_mins = np.reshape(w_mins, (frbs.shape[0], _observation_len))

  consequents_mtx = frbs[:,-1]

  A = np.array([consequents_mtx[:,0]]).T
  B = np.array([consequents_mtx[:,1]]).T
  C = np.array([consequents_mtx[:,2]]).T
  D = np.array([consequents_mtx[:,3]]).T

  _numerator_mtx = 3*w_mins * (D**2 - A**2) * (1-w_mins) + 3 * w_mins**2 * (C*D - A*B) + w_mins**3 * (C-D+A-B)*(C-D-A+B)
  _denominator_mtx = 2*w_mins * (D-A) + w_mins**2 * (C + A - D - B)

  Nu = np.sum(_numerator_mtx)
  De = np.sum(_denominator_mtx)

  return Nu, De

def get_numerator_and_denominator_self(self):
  return get_numerator_and_denominator_independent(frbs=self.model, X=self.inp.observations)

def explicit_formula_predictions(self):
  '''
    Example:
    --------
    
    >>> observations = np.array([[2.6, 2.4]])
    >>> inp = config2input(conf, observations, desired_outputs)
    >>> inp.boundaries = [[0,7], [0,7], [0,7]]
    >>> bact = Bacterium(inp=inp)
    >>> bact.model = np.array([[[1,2,2,3],[1,3,3,5],[3,4,4,5]], [[2,4,4,6],[1,2,2,3],[4,5.5,5.5,7]]])
    >>> bact.explicit_formula_predictions()
    
    explicit_formula_predictions output: 4.816725978647686
    mamdaniInference output: [4.81672598]
  '''
  N, S = get_numerator_and_denominator_self(self)
  return  ((1/3) * (N/S))

def explicit_formula_predictions_independent(frbs, X):
  N, S = get_numerator_and_denominator_independent(frbs, X)
  return  ((1/3) * (N/S))