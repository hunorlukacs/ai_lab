import copy
import numpy as np
from BMA_FUZZY.fuzzy.w_min import w_min
from BMA_FUZZY.fuzzy.trapmf import trapmf
from BMA_FUZZY.bma.bacterium_module._exlpicit_formula import explicit_formula_predictions_independent
from BMA_FUZZY.bma.bacterium_module._exlpicit_formula import get_numerator_and_denominator_independent

def N_ij(start, end, x):
  # WARN: include/exclude boundaries
  return 1 if x >= start and x <= end else 0

def jacobian_matrix_analytic_self(self):
  return jacobian_matrix_analytic(self.model, self.inp.observations)

def jacobian_matrix_analytic(frbs, X) -> np.ndarray:
  '''
  Calculates the Jacobian matrix analitically.

  Note: see also jacobian_matrix_numeric() - for the numerical calculations

  Parameters:
  --------
    frbs: (R, O+1, 4) 
        FRBS
    X: (N, O)
        observations

  Returns:
  --------
  J: (N, R*(O+1)*4)
        Jacobian matrix

  Example:
  -------
  >>> observations = np.array([[2.6, 2.4], [1.5, 2]])
  >>> inp = config2input(conf, observations, desired_outputs)
  >>> inp.boundaries = [[0,7], [0,7], [0,7]]
  >>> bact = Bacterium(inp=inp)
  >>> bact.model = np.array([[[1,2,2.1,3],[1,3,3.1,5],[3,4,4.1,5]], [[2,4,4.1,6],[1,2,2.1,3],[4,5.5,5.6,7]]])
  >>> bact.jacobian_matrix_analytic()
    
  >>> # array([[-0.        ,  0.        , -0.29919326, -0.37399157,  0.        ,
  >>> #        0.        , -0.        ,  0.        ,  0.3742471 ,  0.10053214,
  >>> #       -0.00402963,  0.00604245, -0.36580229, -0.15677241,  0.        ,
  >>> #       -0.        ,  0.        , -0.        ,  0.        ,  0.        ,
  >>> #        0.10083499,  0.01509468,  0.05894995,  0.34832831],
  >>> #      [-0.00950013, -0.00950013,  0.        , -0.        , -0.00475007,
  >>> #       -0.00475007,  0.        , -0.        ,  0.38802293,  0.11112604,
  >>> #        0.11291767,  0.38793335, -0.        ,  0.        ,  0.        ,
  >>> #       -0.        ,  0.        , -0.        ,  0.        , -0.        ,
  >>> #        0.        ,  0.        ,  0.        ,  0.        ]])
  '''

  nr_rules, nr_trapmf, _ = frbs.shape 
  R = nr_rules
  n = nr_trapmf-1 # nr of antecedents / dim. of the observation
  J = np.empty((len(X), 4*(n+1)*R))
  J_row, J_col = 0, 0


  for x_p in X:
    S, N = get_numerator_and_denominator_independent(frbs, np.array([x_p]))
    for i, rule in enumerate(frbs):
      # w - is w_min
      W = w_min(rule, np.array([x_p]))

      ''' CONSEQUENT's attributes '''
      A, B, C, D = rule[-1][0], rule[-1][1], rule[-1][2], rule[-1][3]

      dF_dW = 3*(D**2-A**2)*(1-2*W) + 6*W*(C*D-A*B) + 3*W**2*((C-D)**2 - (A-B)**2)
      dG_dW = 2*(D-A) + 2*W*(C+A-D-B)

      if N == 0:
        dy_dW = 0
      else:          
        dy_dW = (1/3) * (N*dF_dW - S*dG_dW) / N**2

      '''Antecedents'''
      for j in range(n): # iterate through the ANTECEDENTS
        trap = rule[j]
        a,b,c,d = trap[0], trap[1], trap[2], trap[3]
        x = x_p[j]

        dmu_da = (x-b) / (b-a)**2 * N_ij(a,b,x)
        dmu_db = (a-x) / (b-a)**2 * N_ij(a,b,x)
        dmu_dc = (d-x) / (d-c)**2 * N_ij(c,d,x)
        dmu_dd = (x-c) / (d-c)**2 * N_ij(c,d,x)
      
        dW_dmu = 1 if trapmf(np.array([x]), abcd=trap) == W else 0

        dy_da = dy_dW*dW_dmu*dmu_da
        dy_db = dy_dW*dW_dmu*dmu_db
        dy_dc = dy_dW*dW_dmu*dmu_dc
        dy_dd = dy_dW*dW_dmu*dmu_dd

        J[J_row][J_col] = dy_da
        J_col = J_col+1
        J[J_row][J_col] = dy_db
        J_col = J_col+1
        J[J_row][J_col] = dy_dc
        J_col = J_col+1
        J[J_row][J_col] = dy_dd
        J_col = J_col+1

      '''Consequent'''
      dt_F_A = -6*W*A + 6*W**2*A - 3*W**2*B - 2*W**3*(A-B)
      dt_F_B = -3*W**2*A + 2*W**3*(A-B)
      dt_F_C = 3*W**2*D - 2*W**3*(D-C)
      dt_F_D = 6*W*D - 6*W**2*D + 3*W**2*C + 2*W**3*(D-C)

      dt_G_A = -2*W + W**2
      dt_G_B = -W**2
      dt_G_C = W**2
      dt_G_D = 2*W - W**2

      if N == 0:
        dy_dA=0
        dy_dB=0
        dy_dC=0
        dy_dD=0
      else:
        dy_dA = (1/3) * (N*dt_F_A - S*dt_G_A) / N**2
        dy_dB = (1/3) * (N*dt_F_B - S*dt_G_B) / N**2
        dy_dC = (1/3) * (N*dt_F_C - S*dt_G_C) / N**2
        dy_dD = (1/3) * (N*dt_F_D - S*dt_G_D) / N**2

      J[J_row][J_col] = dy_dA
      J_col = J_col+1
      J[J_row][J_col] = dy_dB
      J_col = J_col+1
      J[J_row][J_col] = dy_dC
      J_col = J_col+1
      J[J_row][J_col] = dy_dD
      J_col = J_col+1

    J_row = J_row+1
    J_col = 0
  return J


def jacobian_matrix_numeric(self):
  '''
    Example:
    -------
    >>> observations = np.array([[2.6, 2.4], [1.5, 2]])
    >>> inp = config2input(conf, observations, desired_outputs)
    >>> inp.boundaries = [[0,7], [0,7], [0,7]]
    >>> bact = Bacterium(inp=inp)
    >>> bact.model = np.array([[[1,2,2.1,3],[1,3,3.1,5],[3,4,4.1,5]], [[2,4,4.1,6],[1,2,2.1,3],[4,5.5,5.6,7]]])
    >>> bact.jacobian_matrix_numeric()
    
    >>> #array([[ 0.        ,  0.        , -0.29976022, -0.37392311,  0.        ,
    >>> #     0.        ,  0.        ,  0.        ,  0.37481129,  0.10080825,
    >>> #    -0.00444089,  0.00577316, -0.36592951, -0.15676349,  0.        ,
    >>> #     0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
    >>> #     0.10036416,  0.01509903,  0.05906386,  0.34816594],
    >>> #   [-0.00444089, -0.00444089,  0.        ,  0.        , -0.00222045,
    >>> #    -0.00222045,  0.        ,  0.        ,  0.38724579,  0.11057821,
    >>> #     0.11324275,  0.38724579,  0.        ,  0.        ,  0.        ,
    >>> #     0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
    >>> #     0.        ,  0.        ,  0.        ,  0.        ]])
  '''

  eps = 1e-12
  original_shape = self.model.shape
  parameters = self.model.reshape(-1) 
  observations = self.inp.observations
  func = explicit_formula_predictions_independent

  J = np.zeros((len(observations), len(parameters)))

  for x_id in range(len(observations)):
    for p_id in range(len(parameters)):

      parameters_upper = parameters.copy()
      parameters_lower = parameters.copy()
      parameters_upper[p_id] += eps
      parameters_lower[p_id] -= eps
      parameters_upper = parameters_upper.reshape(original_shape)
      parameters_lower = parameters_lower.reshape(original_shape)

      y_upper = func(parameters_upper, X=np.array([observations[x_id]]))
      y_lower = func(parameters_lower, X=np.array([observations[x_id]]))
      J[x_id][p_id] = (y_upper - y_lower) / (2*eps)

  return J