from BMA_FUZZY import Input
from BMA_FUZZY.fuzzy.err import err
from BMA_FUZZY.fuzzy.mamdani_inference import mamdaniInference
import numpy.random as rnd
from BMA_FUZZY.fuzzy.plot_frbs import plot_frbs
import numpy as np


def plot_fuzzyRBS(self, obs=None):
  '''
    Draws the FRBS system according to the data attribute
  '''
  plot_frbs(frbs=self.model, boundaries=self.inp.boundaries, PADDING_RATE=self.inp.PADDING_RATE , obs=obs)


def predict(self, Xs):
  return mamdaniInference(frbs=self.model, observations=Xs)

def iloc(n_cols, id):
  '''
    Return the row and column coordinates corresponding to the id

    Example:
    -------
    >>> id = 5
    >>> r, c = iloc(bact.model.shape[1], id)  # bact.shape = (2, 3, 4)
    >>> bact.model[r][c] = [0,0,0,0]
    >>> # [[[-0.02744361  0.71024438  0.88209296  1.06175258]
    >>> #    [-0.10807391  0.42715272  1.20787205  1.26465176]
    >>> #    [-0.11761359 -0.07167822 -0.01655115  0.37711118]]
    >>> #  [[-0.17603617  0.32945382  0.95020354  1.29010473]
    >>> #    [-0.29505181  0.29890862  0.56411458  0.67474917]
    >>> #    [ 0.          0.          0.          0.        ]]]


  '''
  row, col = id // n_cols, id % n_cols 
  return  row, col


def create_fuzzyRBS(inp:Input) -> np.ndarray:
  """ 
  Creates a random individum (FRBS)
  Parameters:
  ------------
    inp: Input instance  
  """
  observation_dim, nr_rules = inp.observation_dim, inp.nr_rules

  fuzzyRBS = np.ndarray((nr_rules, observation_dim+1, 4))
  for i in range(nr_rules):
    for j in range(observation_dim+1):
      fuzzyRBS[i][j] = generate_abcd(bound=inp.boundaries[j], PADDING_RATE=inp.PADDING_RATE)
  return fuzzyRBS


def generate_abcd(bound, PADDING_RATE) -> tuple:
  """ Generates [abcd] breakpoint values for trapezoidal membership function """

  padding = PADDING_RATE * (bound[1] - bound[0])
  nums = np.random.uniform(low=bound[0] - padding, high=bound[1] + padding, size=4)
  nums.sort()
  return nums # a, b, c, d


def get_rnd_geneId_lists(gene_nr):
  '''
    Separates the bacterial chromosome into smaller gene-lists.

    Example:
    -------

    >>> get_rnd_geneId_lists(gene_nr=15)
    >>> # array([ 0, 12,  3, 13]), array([10,  1,  2, 11]), array([ 4,  6, 14,  9]), array([8, 7, 5]) 
  '''

  tmp=np.arange(gene_nr)
  rnd.shuffle(tmp)
  split_into = rnd.randint(1, max(3, gene_nr//2))
  tmp = np.array_split(tmp, split_into)
  return tmp

