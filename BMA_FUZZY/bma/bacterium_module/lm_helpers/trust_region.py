from BMA_FUZZY.bma.bacterium_module.lm_helpers.difference_vector import difference_vector, norm_L2
from bma.jacobian_matrix import JacobianMtx
from fuzzy.mamdani_inference import mamdaniInference
from fuzzy.errors.error import error
import numpy.linalg as linalg

def trust_region(b, Xs, Ds, s, J_old, e_old, E_old, E_new):
  '''
    Returns: trust region - r

    Parameters:
    ---------
    b: (N, O+1, 4)
        Bacterium, Frbs
    s: (R, O+1, 4)
        Update vector
    e: (N, )
    Xs: (N, )
    Ds: (N, )
    J: (N, R*(O+1)*4)
        Jacobian matrix

    Returns:
    -------
    r: scalar
        Trust region
  '''
  
  s_flat = s.reshape(-1)
  r = (E_old - E_new) / (E_old - linalg.norm(J_old @ s_flat + e_old))
  return r