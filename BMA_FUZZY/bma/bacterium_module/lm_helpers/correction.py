

import numpy as np


def correction(pi, pj, Dpi, Dpj):
  '''
    If the pi < pj constraint is comporomised, this method can be applied.
    
    Parameters:
    --------
    pi: scalar
        The "pi" point before any modification applied.
    pj: scalar
        The "pj" point before any modification applied.
    Dpi: scalar
        The modification applied on the pi point. (It can be negative as well)
    Dpj: scalar
        The modification applied on the pj point. (It can be negative as well)

    Returns:
    -------
      (pi_new, pj_new): tuple(scalar, scalar)

    Example:
    --------
    For instance the "b" and "c" breakpoints in a FRB trapezoidal membership function.
     - The breakpoint "b" is pi and breakpoint "c" is pj. pi<pj ("b" < "c") constraint should hold.
     - but after the modification, this constraint would not hold:

    >>> correction(pi=2, pj=2.2, Dpi=.1, Dpj=-0.5)
    >>> # (2.0166666666666666, 2.1166666666666667)
  '''

  ### START CODE ###
  if Dpj == Dpi:
    # There was no modification.
    return pi, pj

  g = (pj - pi) / (2 * (Dpj - Dpi))
  pj_next = pj - g * Dpj
  pi_next = pi - g * Dpi
  ### END CODE ###
  return pi_next, pj_next

def correction_simple(b, boundaries, PADDING_RATE):
  '''
    Example:
    -------
    >>> b = np.array([[[1.0,2,3,4], [1,3,4,5], [1,2,3,4]],
    >>>           [[1,6,5,4], [4,6,7,5], [6,5,4,3]]])
    >>> boundaries = np.array([[1,3], [1,8], [3,5]])
    >>> PADDING_RATE = 0.3
    >>> print(correction_simple(b=b, boundaries=boundaries, PADDING_RATE=PADDING_RATE))
    >>> # [[[1.  2.  3.  3.6]
    >>> #   [1.  3.  4.  5. ]
    >>> #   [2.4 2.4 3.  4. ]]
    >>> # [[1.  3.6 3.6 3.6]
    >>> #   [4.  5.  6.  7. ]
    >>> #   [3.  4.  5.  5.6]]]
  '''
  b_new = b.copy()
  b_new.sort(axis=2)
  len_bounds = np.array(list(map(lambda x: x[1]-x[0], boundaries)))
  pad = len_bounds * PADDING_RATE
  lower_boundaries = boundaries[:, 0] - pad 
  upper_boundaries = boundaries[:, 1] + pad
  for i in range(len(boundaries)):
    b_tmp = b_new[:, i]
    b_tmp[b_tmp < lower_boundaries[i]] = lower_boundaries[i]
    b_tmp[b_tmp > upper_boundaries[i]] = upper_boundaries[i]
    b_new[:, i] = b_tmp
  return b_new

def frbs_correction(b, s):
  '''
    If fuzzy breakpoint constraints are comporomised, this method can be applied.
  '''
  ### START CODE ###
  b_shape = b.shape

  b_flat = b.reshape(-1)
  s_flat = s.reshape(-1)

  for i in range(len(b_flat)-1):
    if i != 0 and i % 3 == 0:
      continue
    b_flat[i], b_flat[i+1] = correction(b_flat[i], b_flat[i+1], s_flat[i], s_flat[i+1])

  b_new = b_flat.reshape(b_shape)
  return b_new

  ### END CODE ###