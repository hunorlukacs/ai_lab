from numpy.linalg import norm
import numpy as np

from BMA_FUZZY.Input import Input 

def _gradient_vector(J, e):
  r, c = J.shape
  gradVect = np.zeros(c)
  for j in range(c):
    gradVect[j] = 0
    for k in range(r):
      gradVect[j] += 2*e[k]*J[k][j]
  return gradVect
  

def stopping_crit_reached(k, E_old, E_new, s, b, J, e):
  '''
    Returns: True, if the stopping criteria is satisfied -> the algorithm can be stopped 
    
    Parameters:
    --------
      param k: iteration
      param E_old: old L2 error
      param E_new: new L2 error 
      param s: update_vector
      param b: bacterium / frbs
      param J: Jacobian matrix
      param e: piecewise L2 error
  '''
  # at least 2 steps are required in order to stop
  if k <= 1:
    return False
  τ = 0.0001
  g = _gradient_vector(J, e)
  if E_old - E_new < τ * (1+E_new) and \
      norm(s) < np.sqrt(τ) * (1 + norm(b)) and \
      norm(g) <= np.cbrt(τ) * (1 + abs(E_new)):
    return True
  return False