
from BMA_FUZZY.fuzzy.error_helpers.error import error
from BMA_FUZZY.fuzzy.mamdani_inference import mamdaniInference


def evaluation(b_old, b_new, E_old, E_new):
  '''
    Parameters:
    -----------
    b_old: (R, O+1, 4)
        Original bacterium, Frbs
    b_new: (R, O+1, 4)
        Updated bacterium with s update vector
    X: (N, O)
        Observations
    E_old, E_new: scalar

    Returns:
    ---------
    b_next: (R, O+1, 4)
        Updated (or original) bacterium
    isUpdated: bool
        If the frbs is updated
  '''
  isUpdated = False
  if E_new < E_old:
    b_next = b_new
    isUpdated = True
  else:
    b_next = b_old
  return b_next, isUpdated 