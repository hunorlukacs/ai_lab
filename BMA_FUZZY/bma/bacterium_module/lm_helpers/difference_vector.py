import numpy as np


def difference_vector(Ys, Ds, metric=None):
  if metric == None:
    current_metric = 'L2'
  else:
    current_metric = metric

  if current_metric == 'Canberra':
    raise NotImplementedError('[-] Canberra distance difference vector!')
    pass
  elif current_metric == 'L2':
    return difference_vector_L2(Ys, Ds)
  else:
    raise ValueError(f'[-] There is no such metric!(Available metrics: Canberra, L2)')


def difference_vector_L2(Ys, Ds):
  '''
    Paramters:
    --------
    Ys: (N, )
        Predictions.
    Ds: (N, )
        Desired outputs.

    Returns:
    --------
      e: (N, )
        Errors, i.e. difference vector.
  '''
  e = Ys - Ds
  return e

def norm_L2(e):
  '''
    Parameters:
    -------
    e: (N, )
        Difference vector.

    Returns:
    -------
    E: scalar
        2-norm sum of squared error.
  '''
  E = np.linalg.norm(e)
  return E
