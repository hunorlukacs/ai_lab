import numpy as np
from BMA_FUZZY.bma.bacterium_module._jacobian_matrix import jacobian_matrix_analytic
from BMA_FUZZY.fuzzy.mamdani_inference import mamdaniInference 
from numpy.linalg import inv

def update_vector(J, e, γ, obs_dim):
  '''
    Calculates the update vector: s

    Parameters:
    -------
      J: (N, R*(O+1)*4)
          Jacobian matrix
      e: (N, )
          Errors, i.e. difference vector.
      γ: scalar 
          Bravery factor

    Returns:
    ---------
      s: (R, O+1, 4)
        Update vector
  '''

  _, c = J.shape
  I = np.eye(c)
  # M = R*(O+1)*4
  # [(M, N) @ (N, M) + (M, M)] @ (M, N) @ (N, )
  # (M, M) @ (M, N) @ (N, )
  # (M, N) @ (N, )
  # (M, )
  s = -inv((J.T @ J + γ*I)) @ J.T @ e

  # c = R*(O+1)*4
  O = obs_dim
  R = int((c / 4) / (O+1))
  s = s.reshape((R, O+1, 4))
  return s