import numpy as np
from BMA_FUZZY.bma.bacterium_module.lm_helpers.bravery_factor import bravery_factor
from BMA_FUZZY.bma.bacterium_module.lm_helpers.correction import frbs_correction
from BMA_FUZZY.bma.bacterium_module.lm_helpers.difference_vector import difference_vector, norm_L2
from BMA_FUZZY.bma.bacterium_module.lm_helpers.evaluation import evaluation
from BMA_FUZZY.bma.bacterium_module.lm_helpers.stopping_criteria import stopping_crit_reached
from BMA_FUZZY.bma.bacterium_module.lm_helpers.trust_region import trust_region
from BMA_FUZZY.bma.bacterium_module.lm_helpers.update_vector import update_vector
from BMA_FUZZY.fuzzy.mamdani_inference import mamdaniInference
from BMA_FUZZY.helpers.helper_functions.generate_rand_indeces import generate_rand_indeces
from BMA_FUZZY.bma.bacterium_module._jacobian_matrix import jacobian_matrix_analytic
import numpy.random as rnd

def levenberg_marquardt(self):
  '''
    Executes the Levenberg-Marquardt optimization algorithm on the individual.
  '''
  if rnd.rand() > self.inp.lm_prob:
    return self

  gamma_init, SUBSAMPLING_ENABLED, obs_dim = self.inp.gamma_init, self.inp.SUBSAMPLING_ENABLED, self.inp.observation_dim
  b, inp = self.model, self.inp

  k=0
  gamma = gamma_init
  s, J, e = None, None, None
  STOPPING_CRITERIA_SATISFIED = False

  ### Subsampling ###
  if SUBSAMPLING_ENABLED:
    subsamp_ind = generate_rand_indeces(inp=inp)
    Xs, Ds = inp.observations[subsamp_ind], inp.desired_outputs[subsamp_ind]
  else:
    subsamp_ind = None
    Xs, Ds = inp.observations, inp.desired_outputs
  ### Subsampling ###

  ### START CODE ###
  while k < inp.lm_iter and not STOPPING_CRITERIA_SATISFIED:
    Ys = mamdaniInference(frbs=b, observations=Xs)
    e = difference_vector(Ys, Ds, metric='L2')
    E = norm_L2(e=e)
    J = jacobian_matrix_analytic(frbs=b, X=Xs)
    s = update_vector(J=J, e=e, γ=gamma, obs_dim=obs_dim)    

    # b_new = frbs_correction(b, s)
    b_new = (b+s)
    b_new.sort(axis=2)

    Ys_new = mamdaniInference(frbs=b_new, observations=Xs)
    e_new = difference_vector(Ys=Ys_new, Ds=Ds)
    E_new = norm_L2(e=e_new)

    r = trust_region(b=b, Xs=Xs, Ds=Ds, s=s, e_old=e, E_old=E, E_new=E_new, J_old=J)
    gamma = bravery_factor(gamma=gamma, r=r)
    
    b, isUpdated = evaluation(b_old=b, b_new=b_new, E_old=E, E_new=E_new)

    # for the next stopping_crit
    if isUpdated:
      J_next = jacobian_matrix_analytic(frbs=b, X=Xs)
      e_next = e_new
    else:
      J_next = J
      e_next = e
    
    k = k+1

    STOPPING_CRITERIA_SATISFIED = stopping_crit_reached(k=k, E_old=E, E_new=E_new, s=s, b=b, J=J_next, e=e_next)
  ### END CODE ###

  self.model = b
  self.error = np.nan
  return self
