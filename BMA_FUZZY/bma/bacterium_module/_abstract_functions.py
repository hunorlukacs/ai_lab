
import numpy as np
from BMA_FUZZY.bma.bacterium_module._helper_functions import create_fuzzyRBS, generate_abcd
from BMA_FUZZY.fuzzy.err import err

def create_model(self):
  return create_fuzzyRBS(inp=self.inp)

def gene_mutation(self, geneIds:list) -> bool:
  '''
    Executes one mutation on the bacterium's chromosome.
    A gene is a trapeze in this case.
    Genes to be mutated is indicated by the geneIds.

    Example:
    --------
    >>> bact.gene_mutation(geneIds=[0,2,5])

    Before mutation:
    >>> [[[-0.16631336  0.02679845  0.75969083  1.14002478]
    >>>   [-0.2180788   0.52908888  0.70392299  1.21983125]
    >>>   [-0.08116438  0.76891793  0.84151403  0.95139908]]
    >>> [[ 0.5745731   1.0011361   1.12128899  1.12902682]
    >>>   [-0.2851651   0.60733604  0.92109484  1.03081071]
    >>>   [ 0.40790661  0.88519169  1.01457366  1.23751101]]]

    After mutation:
    >>> [[[ 0.13845396  0.29421613  0.9271529   0.96553336]
    >>>   [-0.2180788   0.52908888  0.70392299  1.21983125]
    >>>   [ 0.48625684  0.86692467  0.98577376  1.25570414]]
    >>> [[ 0.5745731   1.0011361   1.12128899  1.12902682]
    >>>   [-0.2851651   0.60733604  0.92109484  1.03081071]
    >>>   [-0.10221652  0.41860011  0.93923228  1.24910757]]]
  '''

  r, c, _  = self.model.shape
  boundaries = self.inp.boundaries
  assert all(x < r*c for x in geneIds) and all(x >= 0 for x in geneIds), '[-] Value error: {} ahs to >0 and <{}'.format(geneIds, r*c)
  
  new_genes = [generate_abcd(bound=boundaries[geneIdx % c], PADDING_RATE=self.inp.PADDING_RATE) for geneIdx in geneIds]
  self.model = self.model.reshape((r*c, 4))
  self.model[geneIds] = new_genes
  self.model = self.model.reshape((r, c, 4))

  self.error = np.nan
  return True

def get_chromosome_length(self):
  '''
    Returns the number of genes in the choromosome
  '''
  r, c, _ = self.model.shape
  return r*c


def get_genes(self, geneIds:list) -> np.ndarray:
  '''
  For the given geneIds, it returns the corresponding genes

  Example:
  ------
  >>> self.get_genes(geneIds=[1, 5])

  individum:
  >>> [[[-0.28982908  0.02034273  0.19752207  0.86063849]
  >>>   [ 0.26538729  0.41291067  1.01606416  1.27160476]
  >>>   [-0.24982119  0.10665576  0.16018408  0.28796984]]
  >>> [[ 0.03970868  0.45505592  0.80588281  1.03641604]
  >>>   [-0.20483442  0.07690532  0.24559054  0.6430003 ]
  >>>   [ 0.36286335  0.95582391  1.23937321  1.26342977]]]

  output:
  >>>  [[0.26538729 0.41291067 1.01606416 1.27160476]
  >>>   [0.36286335 0.95582391 1.23937321 1.26342977]]
  '''


  r, c, _  = self.model.shape
  assert all(x < r*c for x in geneIds) and all(x >= 0 for x in geneIds), '[-] Value error: {} ahs to >0 and <{}'.format(geneIds, r*c)
  
  self.model = self.model.reshape((r*c, 4))
  result_genes = self.model[geneIds] 
  self.model = self.model.reshape((r, c, 4))
  return result_genes


def set_genes(self, geneIds:list, new_genes:np.ndarray):
  '''
    Updates it's genes on geneIds localtion with the values of new_genes.

    Note:
    ------
    It updates the error value to NaN!

    Example:
    -------
    
    >>> set_genes(geneIds=[0,5], new_genes=np.array([[0,0,0,0], [0,0,0,0]])

    before the update
    >>> [[[-0.18304855  0.13666382  0.22682816  0.88039112]
    >>>   [-0.25896107 -0.19947514  0.07703904  0.96287034]
    >>>   [ 0.23487434  0.55057925  1.0785298   1.28955343]]
    >>> [[-0.08572487  0.83225531  1.09401513  1.28561429]
    >>>   [-0.01155135  0.54873825  0.70040852  0.99695647]
    >>>   [-0.2877292  -0.0184297   0.05425889  1.0982447 ]]]

    after the update:
    >>> [[[ 0.          0.          0.          0.        ]
    >>>   [-0.25896107 -0.19947514  0.07703904  0.96287034]
    >>>   [ 0.23487434  0.55057925  1.0785298   1.28955343]]
    >>> [[-0.08572487  0.83225531  1.09401513  1.28561429]
    >>>   [-0.01155135  0.54873825  0.70040852  0.99695647]
    >>>   [ 0.          0.          0.          0.        ]]]
  '''

  r, c, _  = self.model.shape
  assert all(x < r*c for x in geneIds) and all(x >= 0 for x in geneIds), '[-] Value error: {} ahs to >0 and <{}'.format(geneIds, r*c)
  
  self.model = self.model.reshape((r*c, 4))
  self.model[geneIds] = new_genes
  self.model = self.model.reshape((r, c, 4))

  self.error = np.nan
  return True

def get_err(self, indeces=None):
  ''' Returns the error (one value) depending on the chosen metric and filtering indeces'''
  return err(individum=self, inp=self.inp, indeces=indeces, metric=self.inp.METRIC_DISTANCE)
