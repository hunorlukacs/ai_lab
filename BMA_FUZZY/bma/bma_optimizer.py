from BMA_FUZZY.Input import Input, input_set_fitData
from BMA_FUZZY.bma.population_module.Population import Population
from BMA_FUZZY.fuzzy.err import err
from BMA_FUZZY.bma.bacterium_module._helper_functions import create_fuzzyRBS
from tqdm import trange
from bea.bacterium_modul.BacteriumAbstract import BacteriumAbstract

from bea.bea_optimizer import BEA_optimizer
from bea.population_module.PopulationAbstract import PopulationAbstract


class BMA_optimizer(BEA_optimizer):
  def __init__(self, inp: Input, MyBacteriumConcreteClass: BacteriumAbstract, MyPopulationConcreteClass:PopulationAbstract) -> None:
    super().__init__(inp=inp, MyBacteriumConcreteClass=MyBacteriumConcreteClass)
    self.MyPopulationConcreteClass = MyPopulationConcreteClass

  def fit(self, observations, desired_outputs=None):
    ''' 
      Bacterial evolutionary optimizer. 

      As the method ends, the following will be available:  will become available:
        - two attributes: `self.solution`, `self.population`
        - methods: `predict()`, `get_solution()`, 


      Note:
      -------    
      The solution is the best individual of the population
    '''

    if not (desired_outputs is None): # if desired output is required
      assert len(observations) == len(desired_outputs), 'The length \
        of the observations and the length of the desired_outputs has to match.\
        Current lengths: observations: {}, desired_outputs: {}'.format(len(observations), len(desired_outputs))
      
      desired_outputs = desired_outputs.reshape((len(desired_outputs), ))
    
    # Creates an Input object according to the configuration. This will be the input of the evolutionary algorithm. 
    self.inp = inp = input_set_fitData(inp=self.inp, observations=observations, desired_outputs=desired_outputs)    

    t = trange(inp.n_gen, desc='Error: Unknown', leave=True)

    for _ in t: # iterate through the generations
      if inp.b_mut: # bacterial mutation
        self.population.mutation()

      if inp.b_lm:  # levenberg-marquardt
        self.population.levenberg_marquardt()

      if inp.b_gt:  # gene transfer
        self.population.gene_transfer()

      best_individum = min(self.population.population, key=lambda bacterium: bacterium.error)
      
      t.set_description(f'Error: {best_individum.error}')
      t.refresh() # to show immediately the update

    self.solution = best_individum 
