from BMA_FUZZY.Input import Input
from BMA_FUZZY.bma.bacterium_module.Bacterium import Bacterium
from bea.bacterium_modul.BacteriumAbstract import BacteriumAbstract
from bea.population_module.PopulationAbstract import PopulationAbstract
import process_pool

def smap(f):
  return f()

class Population(PopulationAbstract):
  def __init__(self, inp:Input, MyBacteriumConcreteClass:BacteriumAbstract) -> None:
    ''' Initializing the population '''
    super().__init__(inp=inp, MyBacteriumConcreteClass=MyBacteriumConcreteClass)


  def mutation(self):
    ''' 
    Performs the Bacterial Mutation operation on the population.

    Parameters:
    -------
    inp: Input object

    Note:
    --------
    If inp.MULTI PROCESS ENABLED is True, certain operations will be executed simultaneously, thus speeding up the whole process.   
    
    Example #1:
    ----------
    >>>  pop = Population(inp=inp)
    >>>  pop.mutation()
    >>> # [{individual Index}, {individual error}]
     BEFORE mutation: 
    >>> # population: [[0, 4.78], [1, 1.92], [2, 2.57], [3, 3.86], [4, 4.72], [5, 3.46], [6, 2.0], [7, 3.14], [8, 2.39], [9, 4.97]]
     AFTER mutation:
    >>> # population: [[0, 2.13], [1, 1.92], [2, 2.13], [3, 2.13], [4, 2.13], [5, 3.46], [6, 2.0], [7, 3.14], [8, 2.39], [9, 4.31]]
    
    Example #2:
    ---------
    n_ind = 10000, nr_rules = 2, n_clone = 2

    Multiprocessing:
      >>> # execution time: 3.7200684547424316 seconds
    Sequential:
      >>> # execution time: 12.870232820510864 seconds
    '''
    return super().mutation()

  def gene_transfer(self):
    '''
      Performs the Gene Transfer operation on the population.

      Note:
      ------
      During this operation, the population is sorted several times according to the error values of the individuals!
      But it doesn't mean that at the end the population will be sorted for sure (because of the last gene transfer).

      Example:
      -------
      >>>  pop = Population(inp=inp)
      >>>  pop.gene_transfer()
      >>> # [{individual Index}, {individual error}]
        BEFORE gene transfer
      >>> # population: [[0, 2.33], [1, 2.43], [2, 3.09], [3, 3.42], [4, 3.47], [5, 3.58], [6, 3.95], [7, 3.96], [8, 4.55], [9, 4.59]]
      >>> # donorId: 0
      >>> # acceptorId: 5
        AFTER gene transfer
      >>> # population: [[0, 2.33], [1, 2.43], [2, 3.09], [3, 3.42], [4, 3.47], [5, 3.28], [6, 3.95], [7, 3.96], [8, 4.55], [9, 4.59]]
      >>> # population after sorting: [[0, 2.33], [1, 2.43], [2, 3.09], [3, 3.28], [4, 3.42], [5, 3.47], [6, 3.95], [7, 3.96], [8, 4.55], [9, 4.59]]
     
    '''
    return super().gene_transfer()

  def levenberg_marquardt(self):
    MULTIPROCESS_ENABLED, n_ind = self.inp.MULTIPROCESS_ENABLED, self.inp.n_ind

    ### START CODE ###
    if MULTIPROCESS_ENABLED:
      ''' Multiprocessing '''
      results = process_pool.PROCESS_POOL.map_async(smap, [self.population[i].levenberg_marquardt for i in range(n_ind)])
      for idx, result in enumerate(results.get()):
        self.population[idx] = result

    else:
      ''' Sequential '''
      for idx in range(len(self.population)):
        self.population[idx].mutation()
    ### END CODE ###
