class InputBEA():
  n_gen = 10
  n_ind = 10
  n_clone = 3
  n_inf = 5
  nr_rules = 1
  b_mut=True  # enable bacterial mutation
  b_gt=True   # enable bacterial gene transfer

  observations = None
  desired_outputs = None
  observation_dim = None
  
  SUBSAMPLING_ENABLED = False
  SUBSAMPL_RATIO = 0.3
  MULTIPROCESS_ENABLED = True


def input_set_fitData(inp:InputBEA, observations, desired_outputs):
  ''' Updates the following attributes: `observations`, `desired_outputs`, `observation_dim` '''
  _inp = inp
  _inp.observations = observations
  _inp.desired_outputs = desired_outputs
  _inp.observation_dim = observations.shape[1]
  return _inp
  