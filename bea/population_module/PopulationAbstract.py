
from abc import ABC
import random
from bea.Input import InputBEA
from bea.bacterium_modul.BacteriumAbstract import BacteriumAbstract
import process_pool
from bea._helper_functions import generate_rand_indeces
import numpy.random as rnd

def smap(f):
  return f()

class PopulationAbstract(ABC):

  def __init__(self, inp:InputBEA, MyBacteriumConcreteClass:BacteriumAbstract):
      super().__init__()
      self.inp = inp
      self.MyBacteriumConcreteClass = MyBacteriumConcreteClass # Lazy initialization
      self._population:list[MyBacteriumConcreteClass] = None

  @property
  def population(self):
    ''' population: List[BacteriumAbstract] '''
    if not self._population:
      # Lazy initialization
      self._population = [self.MyBacteriumConcreteClass(inp=self.inp) for _ in range(self.inp.n_ind) ]
    return self._population

  @population.setter
  def population(self, new_population):
    self._population = new_population

  def mutation(self):
    ''' 
    Performs the Bacterial Mutation operation on the population.
    '''

    MULTIPROCESS_ENABLED, n_ind = self.inp.MULTIPROCESS_ENABLED, self.inp.n_ind

    ### START CODE ###
    if MULTIPROCESS_ENABLED:
      ''' Multiprocessing '''
      results = process_pool.PROCESS_POOL.map_async(smap, [self.population[i].mutation for i in range(n_ind)])
      for idx, result in enumerate(results.get()):
        self.population[idx] = result

    else:
      ''' Sequential '''
      for idx in range(len(self.population)):
        self.population[idx].mutation()
    ### END CODE ###

  def gene_transfer(self):
    '''
      Performs the Gene Transfer operation on the population.
    '''
    n_ind, n_inf, SUBSAMPLING_ENABLED = self.inp.n_ind, self.inp.n_inf, self.inp.SUBSAMPLING_ENABLED

    if n_ind == 1:
      ''' if there is only one individual (eq. test purposes) -> geneTransfer is not needed '''
      return

    len_chromosome = self.population[0].get_chromosome_length()

    ''' Subsampling '''
    if SUBSAMPLING_ENABLED:
      subsampl_ind = generate_rand_indeces(self.inp)
    else:
      subsampl_ind=None

    ### START CODE ###
    for _ in range(n_inf):
      ''' original version '''
      self.population.sort(key=lambda indiv: indiv.error)
      # self.population.sort(key=lambda indiv: indiv.error(indeces=subsampl_ind))
      donorId = rnd.choice(range(n_ind//2))
      acceptorId = rnd.choice(range(n_ind//2, n_ind))

      geneIds = random.sample(range(len_chromosome), random.randint(1, len_chromosome-1))  # selecting random genes
      genes2transfer = self.population[donorId].get_genes(geneIds=geneIds)
      self.population[acceptorId].set_genes(geneIds=geneIds, new_genes=genes2transfer)
    ### END CODE ### 

  def getdata_as_dict(self):
    '''
      Returns all the data wich is relevant to save.

      Returns:
      --------
      data: dict
            Contains: population
    '''
    data = {}
    data['population'] = self.population
    data['inp'] = self.inp
    return data

  def setdata_from_dict(self, data):
    '''
      Sets the population according to tha data.

      Parameter:
      ---------
      data: dict
            Contains: population=
    '''
    self.population = data['population']
    return True

  def get_errors(self):
    ''' 
    Returns:
    --------
        list - Each item represents an individual from the population. An item: [{individual Index}, {individual error}]
    '''
    return [[id, indiv.get_err().round(2)] for id, indiv in enumerate(self.population)]
     