
import numpy as np


def create_model(self):
  raise NotImplementedError('[-] Not implemented: create_model()')

def gene_mutation(self, geneIds:list):
  ''' Executes one mutation on the bacterium's chromosome. '''
  raise NotImplementedError('[-] Not implemented: gene_mutation(geneIds)')

def get_chromosome_length(self) -> int :
  '''  Returns the number of genes in the choromosome. '''
  raise NotImplementedError('[-] Not implemented: get_chromosome_length()')

def set_genes(self, geneIds:list, new_genes:np.ndarray):
  ''' Updates it's genes on geneIds localtion with the values of new_genes. '''
  raise NotImplementedError('[-] Not implemented: get_chromosome_length(geneIds, new_genes)')

def get_err(self, indeces=None) -> float:
  ''' Returns the individum's error. '''
  raise NotImplementedError('[-] Not implemented: get_err(indeces)')

def get_genes(self, geneIds:list) -> np.ndarray:
  ''' For the given geneIds, it returns the corresponding genes. '''
  raise NotImplementedError('[-] Not implemented: get_genes(geneIds)')
