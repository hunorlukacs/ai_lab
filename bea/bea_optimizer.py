
import copy
from tqdm import trange
from bea.Input import InputBEA
from bea.bacterium_modul.BacteriumAbstract import BacteriumAbstract
from bea.model_save_load import save_model
from bea.population_module.PopulationAbstract import PopulationAbstract
from bea.model_save_load import load_model
from bea.Input import input_set_fitData

class BEA_optimizer():
  ''' Bacterial evolutionary algorithm optimizer. '''
  def __init__(self, inp:InputBEA, MyBacteriumConcreteClass:BacteriumAbstract) -> None:
    self._inp = inp
    self.MyBacteriumConcreteClass = MyBacteriumConcreteClass
    self.MyPopulationConcreteClass = None
    self._solution = None
    self._population = None # lazy initialization

  @property
  def inp(self):
    return self._inp

  @inp.setter
  def inp(self, new_inp):
    self._inp = new_inp

  @property
  def population(self):
    if self._population is None:
      if self.inp.observations is None or self.inp.desired_outputs is None or self.inp.observation_dim is None:
        raise ValueError(f'[-] Cannot get attribute population, because [observations] is None = {self.inp.observations is None},\
                            [desired_outputs] is None = {self.inp.desired_outputs is None}, [observation_dim] is None = {self.inp.observation_dim is None}.')
      else:
        # Lazy initialization of the population
        if self.MyPopulationConcreteClass is None:
          self._population = PopulationAbstract(inp=self.inp, MyBacteriumConcreteClass=self.MyBacteriumConcreteClass)
        else:
          self._population = self.MyPopulationConcreteClass(inp=self.inp, MyBacteriumConcreteClass=self.MyBacteriumConcreteClass)
    return self._population

  @population.setter
  def population(self, new_population):
    self._population = new_population

  @property
  def solution(self) -> BacteriumAbstract:
    ''' Returns the solution. (Best individual of the population) '''
    if not self._solution:
      raise ValueError('[-] There is no solution yet. Please call the fit function first!')
    return self._solution
  
  @solution.setter
  def solution(self, new_solution):
    self._solution = new_solution

  def fit(self, observations, desired_outputs=None):
    ''' 
      Bacterial evolutionary optimizer. 

      Parameters:
      --------
      observations: (N, O)
          N - number of observations, O - observation dimension
      desired_outputs: (N, D)
          N - number of observations, D - desired output dimension

      Returns:
      -------
      `(solution, population)`: tuple
          solution is the best individual of the population

    '''

    if not (desired_outputs is None): # if desired output is required
      assert len(observations) == len(desired_outputs), 'The length \
        of the observations and the length of the desired_outputs has to match.\
        Current lengths: observations: {}, desired_outputs: {}'.format(len(observations), len(desired_outputs))
      
      desired_outputs = desired_outputs.T

      # desired_outputs = desired_outputs.reshape((len(desired_outputs), ))
    # Creates an Input object according to the configuration. This will be the input of the evolutionary algorithm. 
    self.inp = inp = input_set_fitData(self.inp, observations=observations, desired_outputs=desired_outputs)

    t = trange(inp.n_gen, desc='Error: Unknown', leave=True)

    for _ in t: # iterate through the generations
      
      if inp.b_mut: # bacterial mutation
        self.population.mutation()

      if inp.b_gt:  # gene transfer
        self.population.gene_transfer()

      best_individum = min(self.population.population, key=lambda bacterium: bacterium.error)
      
      t.set_description(f'Error: {best_individum.error}')
      t.refresh() # to show immediately the update

    self.solution = best_individum 
    return self.solution, self.population
    
  def predict(self, Xs):
    ''' Returns the solution's (best individual's) prediction. '''
    if not self.solution:
      raise ValueError('[-] There is no solution yet. Please call the fit function first!')
    return self.solution.predict(Xs=Xs)


  def save(self, path, filename, append_time=True):
    '''
      Saves all the data into a file.

      Keys: 
      -----
      - `population`: List[Bacterium]
          -  The whole population.
      - `solution`: Bacterium
          - Best individual
      - `inp`: Input
          - Configuration file. 

      Example:
      -------
      Save:
      >>> bma.save(path=f'{ROOT_DIR}/data/models/', filename='test')
      >>> # [+] Model successfully saved to: {ROOT_DIR}/data/models//test_20221202-232101.model
      >>> print(bma.get_solution().get_err())   # 0.3291587218408204
      Load:
      >>> b2 = BacterialMemeticAlg4FuzzyRBS(conf=Config())
      >>> b2.load(path='{ROOT_DIR}/data/models//test_20221202-232101.model')  # [+] Model loaded successfully!
      >>> b2.get_solution().get_err() # 0.3291587218408204


    '''
    if self.population is None or self.solution is None:
      raise NameError('[-] Warning: population and/or solution is None! (Maybe optmizition method has not been called yet.) ')
    
    data = {}
    data['population'] = self.population
    data['solution'] = self.solution

    inp_save = copy.copy(self.inp)
    inp_save.observations = None
    inp_save.desired_output = None
    data['inp'] = inp_save
    
    save_model(data=data, directory=path, filename=filename, append_time=append_time)
    return True

  def load(self, path, set_input=False):
    '''
      Loads all the data from a dictionary.

      Keys: 
      -----
      - `population`: List[Bacterium]
          -  The whole population.
      - `solution`: Bacterium
          - Best individual
      - `inp`: Input
          - Input file. 

      See also: save()
    '''
    data = load_model(path=path)
    self.population = data['population']
    self.solution = data['solution']
    if set_input:
      self.inp = data['inp']
    print('[+] Model loaded successfully!')
    return True