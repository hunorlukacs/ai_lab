
import numpy as np
from BMA_FUZZY.BacterialMemeticAlg4FuzzyRBS import BacterialMemeticAlg4FuzzyRBS
from outlier_detection.Config2Input import config2input
from outlier_detection.Config import Config_OD
from outlier_detection.helper_functions import OutlierDetectionManager


def create_outlier_list(Ys, Ds, theta, metric='L2'):
  if metric == 'L2':
    e = (abs(Ds - Ys)>=theta)
    count = np.count_nonzero(e)
  return e, count

class OutlierDetector():
  def __init__(self, observations, desired_outputs, config:Config_OD) -> None:
    self.observations, self.desired_outputs = observations, desired_outputs
    self.conf = config
    self.inp = config2input(self.conf, observations, desired_outputs) 

  def createFRBbyBMA(self, rule):
    Xs, Ds = self.observations, self.desired_outputs
    self.conf.nr_rules = rule

    bma = BacterialMemeticAlg4FuzzyRBS(conf=self.conf)
    bma.fit(Xs, Ds)
    return bma.get_solution().model

  def run(self):
    minRule, maxRule, nRuns = self.conf.minRule, self.conf.maxRule, self.conf.nRuns
    Xs, Ds = self.observations, self.desired_outputs
    THETA = self.conf.THETA
    current_conf = Config_OD()
    current_conf = self.conf
    self.menegers = {}

    FuzzyRuleBase = {}
    ErrorMatrix = {}
    OutlierList = {}
    OutlierArray = {}
    AverageErrors = {}
    optRule = 0

    for rule in range(minRule, maxRule+1):

      current_conf.nr_rules=rule
      FuzzyRuleBase[rule] = {}
      ErrorMatrix[rule] = {}
      OutlierArray[rule] = {}
      self.menegers[rule] = {}

      for run in range(1, nRuns+1):
        
        manager = OutlierDetectionManager(current_conf, Xs, Ds)
        self.menegers[rule][run] = manager

        FuzzyRuleBase[rule][run] = manager.createFRBbyBMA(rule=rule)
        ErrorMatrix[rule][run] = manager.evalFRBS()
        Ys = manager.predict(Xs=Xs)
        OutlierList, OutlierSum = create_outlier_list(Ys=Ys, Ds=Ds, theta=THETA)
        print(f'OutlierSum: {OutlierSum}')
        OutlierArray[rule][run] = OutlierList

      sum_ErrorMatrix = 0
      for i in range(1, nRuns+1):
        sum_ErrorMatrix += ErrorMatrix[rule][i]
      AverageErrors[rule] = (1/nRuns) * sum_ErrorMatrix

    ErrorDrop = AverageErrors[minRule] - AverageErrors[minRule+1]
    maxErrorDrop = ErrorDrop
    optRule = minRule
    for rule in range(minRule+1, maxRule):
      ErrorDrop = AverageErrors[rule] - AverageErrors[rule+1]
      if ErrorDrop > maxErrorDrop:
        maxErrorDrop = ErrorDrop
        optRule = rule
    OutlierList = OutlierArray[optRule][1]
    for run in range(2, nRuns+1):
      OutlierList = np.array(OutlierList)&np.array(OutlierArray[optRule][run])

    self.optRule = optRule
    self.OutlierList, self.FuzzyRuleBase, self.ErrorMatrix, self.AverageErrors = OutlierList, FuzzyRuleBase, ErrorMatrix, AverageErrors
    return True
