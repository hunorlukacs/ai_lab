import numpy as np
from Input import Input
from fuzzy.errors.error_canberra import error_canberra, error_canberra_piecewise
from fuzzy.mamdani_inference import mamdaniInference
from bma.bacterialEvolution import bacterialEvolution

def create_outlier_list(Ys, Ds, theta):
  e = (error_canberra_piecewise(Ys,Ds)>=theta)
  return e, np.count_nonzero(e)


def outlierDetection(minRule, maxRule, nRuns, inp:Input):
  FuzzyRuleBase = {}
  ErrorMatrix = {}
  OutlierList = {}
  OutlierArray = {}
  AverageErrors = {}
  optRule = 0
  THETA = 0.1

  for rule in range(minRule, maxRule+1):
    inp.nr_rules=rule
    FuzzyRuleBase[rule] = {}
    ErrorMatrix[rule] = {}
    OutlierArray[rule] = {}
    for run in range(1, nRuns+1):
      FuzzyRuleBase[rule][run] = bacterialEvolution(inp)
      Ys=mamdaniInference(frbs=FuzzyRuleBase[rule][run], observations=inp.observations)
      ErrorMatrix[rule][run] = error_canberra(Ys=Ys, Ds=inp.desired_outputs)
      OutlierList, OutlierSum = create_outlier_list(Ys=Ys, Ds=inp.desired_outputs, theta=THETA)
      print(f'OutlierList: {OutlierList} , OutlierSum: {OutlierSum}')
      OutlierArray[rule][run] = OutlierList

    sum_ErrorMatrix = 0
    for i in range(1, nRuns+1):
      sum_ErrorMatrix += ErrorMatrix[rule][i]
    AverageErrors[rule] = (1/nRuns) * sum_ErrorMatrix

  ErrorDrop = AverageErrors[minRule] - AverageErrors[minRule+1]
  maxErrorDrop = ErrorDrop
  optRule = minRule
  for rule in range(minRule+1, maxRule):
    ErrorDrop = AverageErrors[rule] - AverageErrors[rule+1]
    # print(f'Rule: {rule} - ErrorDrop:  {ErrorDrop}')
    if ErrorDrop > maxErrorDrop:
      maxErrorDrop = ErrorDrop
      optRule = rule
  OutlierList = OutlierArray[optRule][1]
  for run in range(2, nRuns+1):
    OutlierList = np.array(OutlierList)&np.array(OutlierArray[optRule][run])
  # print('OutlierList:', OutlierList)
  # print('optRule: ', optRule)
  return OutlierList, FuzzyRuleBase