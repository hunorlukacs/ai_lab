import numpy as np


def get_boundaries(obs: np.ndarray, desired_output: np.ndarray) -> np.ndarray:
  '''
    Returns the boundaries (min and max values) for each column in the observation dataset.

    Example:
    ------
    >>> obs = np.array([[0, 5, -2, 1], [-5, 2, -1, 9], [3, -7, 3, 5]])
    >>> des_out = np.array([0, 1])
    >>> get_boundaries(obs) # array([[-5, 3], [-7, 5], [-2, 3], [1, 9],[0, 1]])
  '''

  bound_ante = np.array([obs.min(axis=0), obs.max(axis=0)]).T  # boundaries for antecedents
  result = np.vstack((bound_ante, np.array([min(desired_output), max(desired_output)])))  # append boundaries of the consequent
  return result
