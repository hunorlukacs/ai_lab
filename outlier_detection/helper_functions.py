from BMA_FUZZY.BacterialMemeticAlg4FuzzyRBS import BacterialMemeticAlg4FuzzyRBS
from outlier_detection import Config

class OutlierDetectionManager():
  def __init__(self, conf:Config, Xs, Ds) -> None:
    self.Xs, self.Ds = Xs, Ds
    self.conf = conf

  def createFRBbyBMA(self, rule):
    '''
      Parameter:
        rule: number
            Determines how many rules will be the FRBS built of.
    '''
    Xs, Ds = self.Xs, self.Ds
    self.conf.nr_rules = rule
    bma = BacterialMemeticAlg4FuzzyRBS(conf=self.conf)
    bma.fit(Xs, Ds)
    self.solution = bma.get_solution()
    self.solution_model, self.solution_error = self.solution.model, self.solution.error 
    return self.solution_model
  
  def evalFRBS(self):
    return self.solution_error

  def predict(self, Xs):
    return self.solution.predict(Xs)