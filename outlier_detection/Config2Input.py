from outlier_detection.Input import Input
from outlier_detection.get_boundaries import get_boundaries
from outlier_detection.Config import Config_OD


def config2input(conf:Config_OD, observations, desired_outputs) -> Input:
  '''
    Creates an Input object from a Config object.

    Parameter:
    -------
    conf: Config object

    Returns:
    ------
    inp: Input object

    Notes:
    -------
    All the attributes from Config object will be set or added to the Input object.

  '''
  inp = Input()
  inp = conf

  inp.observations = observations
  inp.desired_outputs = desired_outputs
  inp.observation_dim = observations.shape[1]
  inp.boundaries = get_boundaries(observations, desired_outputs)

  return inp