
class Config(object):
  ''' Config parameters for Bacterial Memetic Algorthm for Fuzzy Rule-Based System '''
  n_gen = 10
  n_ind = 10
  n_clone = 2
  n_inf = 5
  nr_rules = 1
  THETA = .1
  lm_prob = .2
  lm_iter = 10
  gamma_init = .5
  b_mut=True  # enable bacterial mutation
  b_gt=True   # enable bacterial gene transfer
  b_lm=True   # enable the levenberg marquardt on bacterial evolution
  popultaion=None  # in case of loading previous data 
  
  SUBSAMPLING_ENABLED = False
  MULTIPROCESS_ENABLED = True
  PADDING_RATE = 0.3
  # METRIC_DISTANCE = 'Canberra'
  METRIC_DISTANCE = 'L2'
  BACT_MUT_DIVIDE_GENELIST_INTO = 3
  SAVE_DIR = 'data/models/'

class Config_OD(Config):
  ''' Config parameters for the Outlier detectior, based on Bacterial Memetic Algorthm for Fuzzy Rule-Based System '''
  minRule = 2 
  maxRule = 5
  nRuns = 3
  THETA = 0.1