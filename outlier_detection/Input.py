from outlier_detection.Config import Config


class Input(Config):
  ''' Input for Bacterial Memetic Algorthm for Fuzzy Rule-Based System '''
  boundaries = None
  observations = None
  observation_dim = None
  desired_outputs = None